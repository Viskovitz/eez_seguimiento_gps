from PyQt5.QtCore import QVariant
from qgis.core import QgsVectorLayer, QgsProject

import glob
import os
import pandas as pd
import numpy as np

#os.chdir("/mnt/intercambio/_TRABAJO/EEZ/Open2preserve/seguimiento_gps")
os.chdir("D:/_TRABAJO/EEZ/Open2preserve/seguimiento_gps")

capa_parcelas = "geopackage/seguimiento_gps.gpkg|layername=geo_parcelas_wgs84"
iface.addVectorLayer(capa_parcelas, "geo_parcelas_wgs84", "ogr")
#geoparcelas = QgsVectorLayer(capa_parcelas, "geo_parcelas_wgs84", "ogr")
geoparcelas = QgsProject.instance().mapLayersByName("geo_parcelas_wgs84")[0]

for csv in glob.glob("*.csv"):
    # Importo la tabla y creo un data frame
    tabla = pd.read_csv(csv, sep=";")
    
    # Convierto la columna track_time en tipo datetime, que es el equivalente en Pandas a timestamp
    tabla["track_time"] = pd.to_datetime(tabla["track_time"], format="%Y-%m-%d %H:%M:%S")

    # Ordeno la tabla descendente por el campo track_time para calcular
    # la diferencia de tiempo entre un punto y el anterior y que ésta se quede
    # guardada en el registro correspondiente (el anterior ascendente)
    tabla = tabla.sort_values(["track_time"], ascending=False)
    
    # Creo un campo nuevo solo con la fecha
    # Le digo que tiene que ser de tipo cadena para que no dé problemas al exportar a shp o gpkg
    tabla["dia"] = tabla["track_time"].dt.date.astype(str)

    # Calculo la diferencia en minutos entre un registro y el anterior
    tabla["dif"] = round(tabla.track_time.diff() / np.timedelta64(1, "m"), 0) * -1
    
    # Calculo el número de veces que se debe copiar el registro anterior: (diferencia de tiempo/5)-1
    tabla["veces"] = (tabla["dif"].fillna(0.0).astype(int) / 5).astype(int)

    # Quito los valores 0 para que no se eliminen esos registros al duplicar luego
    # tabla.veces = round(tabla.veces.mask(tabla.veces.lt(0),0),0) # Esto es para quitar valores -1 que usaba antes
    tabla.loc[tabla["veces"] == 0, "veces"] = 1

    # Duplico los registros tantas veces como el valor de la columna veces
    tabla = tabla.loc[tabla.index.repeat(tabla["veces"])].reset_index(drop=True)
    
    # La vuelvo a ordenar por fecha (track_time) ascendente
    tabla = tabla.sort_values(["track_time"], ascending=True)
    
    # Exporto el data frame a una tabla para poder convertirla en puntos
    tabla.to_csv("tmp/csv_nuevo_" + csv, sep=";")

    # Represento la tabla usando los camopos latitude y longitude y la cargo como shp
    tablatrat = "file:" + "tmp/csv_nuevo_" + csv + "?delimiter=;&xField=longitude&yField=latitude"
    inp_tab = QgsVectorLayer(tablatrat,"qps_1","delimitedtext")
#    QgsProject.instance().addMapLayer(inp_tab)
    salida = "tmp/shp_nuevo_" + csv.split(".csv",1)[0] + ".shp"
    crs = 4326
    spatRef = QgsCoordinateReferenceSystem(crs, QgsCoordinateReferenceSystem.EpsgCrsId)
    outLayer = QgsVectorFileWriter.writeAsVectorFormat(inp_tab,salida,"UTF-8",spatRef,"ESRI Shapefile")
    # Cargo la capa salida solo si hace falta hacer alguna comprobación
    cargasalida = iface.addVectorLayer(salida, "", "ogr")
    point_lyr = QgsProject.instance().mapLayersByName("shp_nuevo_" + csv.split(".csv",1)[0])[0]
    
    # A PARTIR DE AQUÍ EMPIEZA LA CUENTA DE PUNTOS POR POLÍGONOS
    
    # Indexo los valores del campo dia de la capa de puntos
    field_index = point_lyr.fields().indexFromName("dia")
    
    # Obtengo los valores únicos del campo dia
    dias_unicos = point_lyr.uniqueValues(field_index)
    
    # Guardo capa_parcelas con otro nombre, elimino campos innecesarios y comienzo su edición
    parcelas = QgsVectorFileWriter.writeAsVectorFormat(geoparcelas,"tmp/cuenta_" + csv.split(".csv",1)[0], "utf-8", driverName = "ESRI Shapefile")
#    parcelastmp = QgsVectorLayer("tmp/cuenta_" + csv.split(".csv",1)[0], csv.split(".csv",1)[0], "ogr")
#    parcelascuenta = QgsVectorLayer(parcelas, "parcelas" + csv.split(".csv",1)[0], "ogr")
    iface.addVectorLayer("tmp/cuenta_" + csv.split(".csv",1)[0] + ".shp", "cuenta_" + csv.split(".csv",1)[0], "ogr")
    parcelascuenta = QgsProject.instance().mapLayersByName("cuenta_" + csv.split(".csv",1)[0])[0]
    parcelascuenta.startEditing()
    
    # Se añade un campo a la capa de polígonos por cada clase
    for dia in dias_unicos:
        if parcelascuenta.fields().indexFromName(dia)==-1:
            parcelascuenta.addAttribute(QgsField(dia, QVariant.Int))
#    parcelascuenta.updatefields()
    
    for feat in parcelascuenta.getFeatures():
        g = feat.geometry()
        req = QgsFeatureRequest(g.boundingBox()) # Para mejorar el rendimiento ajustando el área de trabajo
        dias = [p["dia"] for p in point_lyr.getFeatures(req) if g.contains(p.geometry())]
        dias_en_poligono = {cls: dias.count(cls) for cls in dias_unicos}
    
        for cls in dias_en_poligono:
            feat[cls] = dias_en_poligono[cls]
            parcelascuenta.updateFeature(feat)
    
    parcelascuenta.commitChanges()
