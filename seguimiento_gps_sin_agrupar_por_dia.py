#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import os
import geopandas as gpd
import numpy as np
import pandas as pd

# import matplotlib.pyplot as plt
# import datetime

# Establezco el directorio de trabajo en el mismo donde está este archivo .py
os.chdir(os.path.dirname(os.path.abspath(__file__)))

geoparcelas = gpd.read_file('geopackage/seguimiento_gps.gpkg', layer='geo_parcelas_wgs84')
geoparcelas.crs = 'epsg:4326'

geozonas = gpd.read_file('geopackage/seguimiento_gps.gpkg', layer='geo_zonas_wgs84')
geozonas.crs = 'epsg:4326'

georesto = gpd.read_file('geopackage/seguimiento_gps.gpkg', layer='geo_resto_wgs84')
georesto.crs = 'epsg:4326'

geotodo = gpd.read_file('geopackage/seguimiento_gps.gpkg', layer='geo_todo_wgs84')
geotodo.crs = 'epsg:4326'

aprisco = gpd.read_file('geopackage/seguimiento_gps.gpkg', layer='aprisco_mas_grande_50m_wgs84')
aprisco.crs = 'epsg:4326'

# Creo una lista para almacenar los data frames creados y luego guardarlos en un solo csv
parcelas = []
zonas = []
resto = []
todo = []

for csv in glob.glob('*.csv'):
    # PRIMERO LAS PARCELAS

    # Importo la tabla y creo un data frame
    tabla = pd.read_csv(csv, sep=';')

    # Convierto la columna track_time en tipo datetime, que es el equivalente en Pandas a timestamp
    tabla['track_time'] = pd.to_datetime(tabla['track_time'], format='%Y-%m-%d %H:%M:%S')

    # Ordeno la tabla descendente por el campo track_time para calcular
    # la diferencia de tiempo entre un punto y el anterior y que ésta se quede
    # guardada en el registro correspondiente (el anterior ascendente)
    tabla = tabla.sort_values(['track_time'], ascending=False)

    # Creo una nueva columna para marcar los datos tomados por el dron
    # y diferenciarlos de los valores calculados
    # tabla['origen'] = 'real'

    # Creo dos campos nuevos, uno con la fecha y otro con la hora
    # Le digo que tienen que ser de tipo cadena para que no dé problemas al exportar a shp o gpkg
    tabla['dia'] = tabla['track_time'].dt.date.astype(str)
    tabla['hora'] = tabla['track_time'].dt.time.astype(str)

    # Calculo la diferencia en minutos entre un registro y el anterior
    tabla['dif'] = round(tabla.track_time.diff() / np.timedelta64(1, 'm'), 0) * -1

    # Calculo el número de veces que se debe copiar el registro anterior: (diferencia de tiempo/5)-1
    tabla['veces'] = (tabla['dif'].fillna(0.0).astype(int) / 5).astype(int)

    # Quito los valores 0 para que no se eliminen esos registros al duplicar luego
    # tabla.veces = round(tabla.veces.mask(tabla.veces.lt(0),0),0) # Esto es para quitar valores -1 que usaba antes
    tabla.loc[tabla['veces'] == 0, 'veces'] = 1

    # Duplico los registros tantas veces como el valor de la columna veces
    tabla = tabla.loc[tabla.index.repeat(tabla['veces'])].reset_index(drop=True)

    # Duplico los registros tantas veces como el valor de la columna veces si este es menor que 15 por ejemplo
    # mask = tabla['veces'].lt(15)
    # tabla1 = tabla[mask].copy()
    # tabla1 = tabla1.loc[tabla1.index.repeat(tabla1['veces'])]
    # tabla = pd.concat([tabla1, tabla[~mask]]).reset_index(drop=True)

    # La vuelvo a ordenar por fecha (track_time) ascendente
    tabla = tabla.sort_values(['track_time'], ascending=True)

    # Convierto el dataframe tabla en un geodataframe usando las columnas longitud y latitud
    # y estableciendo el crs=WGS84
    geotab = gpd.GeoDataFrame(tabla, crs='epsg:4326', geometry=gpd.points_from_xy(tabla.longitude, tabla.latitude))
    geotab['track_time'] = geotab['track_time'].astype(str)
    puntosdentro = gpd.sjoin(geotab,aprisco,how='inner') # Con estas dos líneas elimino los puntos dentro del aprisco
    geotabla = geotab[~geotab.index.isin(puntosdentro.index)]
    geotabla.to_file('resultados/shp/puntos_corregidos_' + csv.split(".csv", 1)[0] + '.shp', driver='ESRI Shapefile')
    # geotabla.to_file('geopackage/seguimiento_gps.gpkg', layer='puntos_corregidos_' + csv.split(".csv", 1)[0], driver='GPKG')

    # Hago un spatial join entre la capa de puntos y la de parcelas
    # Por alguna razón hay que instalar Rtree para que funcione esto
    dfsjoinp = gpd.sjoin(geoparcelas, geotabla)  # Spatial join de puntos a polígonos
    dfpivotp = pd.pivot_table(dfsjoinp, index='nombre', columns='dia', aggfunc={'dia': len})
    dfpivotp.columns = dfpivotp.columns.droplevel()
    geoparcelaspuntos = geoparcelas.merge(dfpivotp, how='left', on='nombre')

    # Multiplico los valores de las columnas de cada día (que son float64) por 5 minutos
    geoparcelaspuntos[geoparcelaspuntos.select_dtypes(include=['float64']).columns] *= 5

    # Selecciono las columnas float64 para cambiar el tipo a int eliminando los valores null
    # para eliminar todos los decicmales que aparecen cuando son de tipo float64
    colfechas = geoparcelaspuntos.select_dtypes(include='float64').columns.to_list()
    geoparcelaspuntos[colfechas] = geoparcelaspuntos[colfechas].fillna(0).astype('int')

    # Quito columnas sobrantes
    # cols = ['objectid_1', 'obj', 'cod_zona', 'name', 'datetime', 'elevation', 'datetime_', 'point_x', 'point_y',
    #         'point_z', 'parcela', 'oirg_fid', 'shape_leng', 'shape_area', 'trat_pasto', 'trat_quema', 'cod_fecha_',
    #         'objectid_1', 'campo1']
    # geoparcelaspuntos.drop(cols, axis=1, inplace=True)

    # Exporto las capas resultantes del espatial join a un shp (preparado el geopackage en el comentario)
    # geoparcelaspuntos.to_file('geopackage/seguimiento_gps.gpkg', layer='nuevo_' + csv.split('.csv'), driver='GPKG')
    geoparcelaspuntos.to_file('resultados/shp/parcelas_' + csv.split(".csv",1)[0] + '.shp', driver='ESRI Shapefile')
    # geoparcelaspuntos.to_file('geopackage/seguimiento_gps.gpkg', layer='parcelas_' + csv.split(".csv", 1)[0], driver='GPKG')

    # Para tener las tablas de los shp como csv. Esto se podría quitar y exportar las tablas de los shp desde QGIS
    # Convierto el GeoDataFrame en DataFrame, quito el campo geometry, añado el campo disp al principio y exporto
    geoparcelaspuntos = pd.DataFrame(geoparcelaspuntos)
    geoparcelaspuntos.drop('geometry', axis=1, inplace=True)
    geoparcelaspuntos.insert(loc=1, column='disp', value=csv)
    geoparcelaspuntos.to_csv('resultados/csv/parcelas_' + csv, sep=';')

    # Añado el data frame a la lista que he creado al principio para unirlos todos
    parcelas.append(geoparcelaspuntos)

    # AHORA LAS ZONAS

    # Hago un spatial join entre la capa de puntos y la de zonas
    dfsjoinz = gpd.sjoin(geozonas, geotabla)  # Spatial join de puntos a polígonos
    dfpivotz = pd.pivot_table(dfsjoinz, index='Nombre_zon', columns='dia', aggfunc={'dia': len})
    dfpivotz.columns = dfpivotz.columns.droplevel()
    geozonaspuntos = geozonas.merge(dfpivotz, how='left', on='Nombre_zon')

    # Multiplico los valores de las columnas de cada día (que son float64) por 5 minutos
    geozonaspuntos[geozonaspuntos.select_dtypes(include=['float64']).columns] *= 5

    # Selecciono las columnas float64 para cambiar el tipo a int eliminando los valores null
    # para eliminar todos los decicmales que aparecen cuando son de tipo float64
    colfechas = geozonaspuntos.select_dtypes(include='float64').columns.to_list()
    geozonaspuntos[colfechas] = geozonaspuntos[colfechas].fillna(0).astype('int')

    # Exporto las capas resultantes del espatial join a un shp
    geozonaspuntos.drop(['area_ha'], axis=1)
    geozonaspuntos.to_file('resultados/shp/zonas_' + csv.split(".csv",1)[0] + '.shp', driver='ESRI Shapefile')
    # geozonaspuntos.to_file('geopackage/seguimiento_gps.gpkg', layer='zonas_' + csv.split(".csv", 1)[0], driver='GPKG')

    # Para tener las tablas de los shp como csv. Esto se podría quitar y exportar las tablas de los shp desde QGIS
    # Convierto el GeoDataFrame en DataFrame, quito el campo geometry, añado el campo disp al principio y exporto
    geozonaspuntos = pd.DataFrame(geozonaspuntos)
    geozonaspuntos.drop('geometry', axis=1, inplace=True)
    geozonaspuntos.insert(loc=1, column='disp', value=csv)
    geozonaspuntos.to_csv('resultados/csv/zonas_' + csv, sep=';')

    # Añado el data frame a la lista que he creado al principio para unirlos todos
    zonas.append(geozonaspuntos)

    # AHORA EL RESTO

    # Hago un spatial join entre la capa de puntos y la del resto
    dfsjoinr = gpd.sjoin(georesto, geotabla)  # Spatial join de puntos a polígonos
    dfpivotr = pd.pivot_table(dfsjoinr, index='id', columns='dia', aggfunc={'dia': len})
    dfpivotr.columns = dfpivotr.columns.droplevel()
    georestopuntos = georesto.merge(dfpivotr, how='left', on='id')

    # Multiplico los valores de las columnas de cada día (que son float64) por 5 minutos
    georestopuntos[georestopuntos.select_dtypes(include=['int64']).columns] *= 5

    # Selecciono las columnas float64 para cambiar el tipo a int eliminando los valores null
    # para eliminar todos los decicmales que aparecen cuando son de tipo float64
    colfechas = georestopuntos.select_dtypes(include='float64').columns.to_list()
    georestopuntos[colfechas] = georestopuntos[colfechas].fillna(0).astype('int')

    # Exporto las capas resultantes del espatial join a un shp
    georestopuntos.to_file('resultados/shp/resto_' + csv.split(".csv",1)[0] + '.shp', driver='ESRI Shapefile')
    # georestopuntos.to_file('geopackage/seguimiento_gps.gpkg', layer='resto_' + csv.split(".csv",1)[0], driver='GPKG')

    # Para tener las tablas de los shp como csv. Esto se podría quitar y exportar las tablas de los shp desde QGIS
    # Convierto el GeoDataFrame en DataFrame, quito el campo geometry, añado el campo disp al principio y exporto
    georestopuntos = pd.DataFrame(georestopuntos)
    georestopuntos.drop('geometry', axis=1, inplace=True)
    georestopuntos.insert(loc=1, column='disp', value=csv)
    georestopuntos.to_csv('resultados/csv/resto_' + csv, sep=';')

    # Añado el data frame a la lista que he creado al principio para unirlos todos
    resto.append(georestopuntos)

    # AHORA TODA LA SUPERFICIE

    # Hago un spatial join entre la capa de puntos y la del resto
    dfsjoinr = gpd.sjoin(geotodo, geotabla)  # Spatial join de puntos a polígonos
    dfpivotr = pd.pivot_table(dfsjoinr, index='id', columns='dia', aggfunc={'dia': len})
    dfpivotr.columns = dfpivotr.columns.droplevel()
    geotodopuntos = geotodo.merge(dfpivotr, how='left', on='id')

    # Multiplico los valores de las columnas de cada día (que son float64) por 5 minutos
    geotodopuntos[geotodopuntos.select_dtypes(include=['int64']).columns] *= 5

    # Selecciono las columnas float64 para cambiar el tipo a int eliminando los valores null
    # para eliminar todos los decicmales que aparecen cuando son de tipo float64
    colfechas = geotodopuntos.select_dtypes(include='float64').columns.to_list()
    geotodopuntos[colfechas] = geotodopuntos[colfechas].fillna(0).astype('int')

    # Exporto las capas resultantes del espatial join a un shp
    geotodopuntos.to_file('resultados/shp/resto_' + csv.split(".csv", 1)[0] + '.shp', driver='ESRI Shapefile')
    # georestopuntos.to_file('geopackage/seguimiento_gps.gpkg', layer='resto_' + csv.split(".csv",1)[0], driver='GPKG')

    # Para tener las tablas de los shp como csv. Esto se podría quitar y exportar las tablas de los shp desde QGIS
    # Convierto el GeoDataFrame en DataFrame, quito el campo geometry, añado el campo disp al principio y exporto
    geotodopuntos = pd.DataFrame(geotodopuntos)
    geotodopuntos.drop('geometry', axis=1, inplace=True)
    geotodopuntos.insert(loc=1, column='disp', value=csv)
    geotodopuntos.to_csv('resultados/csv/todo_' + csv, sep=';')

    # Añado el data frame a la lista que he creado al principio para unirlos todos
    todo.append(geotodopuntos)

# UNO LOS csv QUE SE HAN IDO CREANDO EN UNO SOLO (parcelas y zonas) para facilitar el análisis

# Uno en uno solo todos los data frames que he ido guardando en la lista creando al principio
parcelas = pd.concat(parcelas).fillna(0)
zonas = pd.concat(zonas).fillna(0)
resto = pd.concat(resto).fillna(0)
todo = pd.concat(todo).fillna(0)

# Listo las columnas en float64 y las convierto en int para eliminar decimales y que sean todas iguales
colparcelas = parcelas.select_dtypes(include='float64').columns.to_list()
parcelas[colparcelas] = parcelas[colparcelas].fillna(0).astype('int')

colzonas = zonas.select_dtypes(include='float64').columns.to_list()
zonas[colzonas] = zonas[colzonas].fillna(0).astype('int')

colresto = resto.select_dtypes(include='int64').columns.to_list()
resto[colresto] = resto[colresto].fillna(0).astype('int')

coltodo = todo.select_dtypes(include='float64').columns.to_list()
todo[coltodo] = todo[coltodo].fillna(0).astype('int')

# Exporto a un archivo .csv
parcelas.to_csv('resultados/csv/parcelas_unidas.csv', sep=';')
zonas.to_csv('resultados/csv/zonas_unidas.csv', sep=';')
resto.to_csv('resultados/csv/resto_unidas.csv', sep=';')
todo.to_csv('resultados/csv/todo_unidas.csv', sep=';')
