# seguimiento_gps

Os mando la carpeta entera con la información de origen que me mandasteis con el script, todos los puntos de los cuatro GPS y las capas de parcelas, zonas y resto.

La versión de Python es la 3.8 y el script trabaja con los módulos **pandas**, **geopandas** y **numpy** que tienen que estar instalados en el entorno de Python.

No hace falta ejecuarlo desde ninguna aplicación, basta hacerlo desde el intérprete Python o el del sistema.

La estructura del directorio es:

- seguimiento_gps.py: script que se ejecuta. Lo he comentado todo para que se veais lo que se hace en cada línea (las líneas que empiezan con #).

- Información original de los GPS

  - GPS_1.csv
  - GPS_2.csv
  - GPS_3.csv
  - GPS_4.csv

- geopackage: en esta carpeta hay un geopackage con las capas de parcelas, zonas y el resto de superficie.

  Este formato es una base de datos abierta que permite guardar información cartográfica y se lee muy rápido por las aplicaciones. Se puede abrir tanto con QGIS como con ArcGIS.

- resultados: aquí se guarda la información que obtiene

  - shp
    - parcelas: muestra los minutos que han estado los GPS en cada parcela por día (sin contar el aprisco).
    - zonas: muestra los minutos que han estado los GPS en cada zona por día (sin contar el aprisco).
    - resto: muestra los minutos que han estado los GPS fuera de las zonas por día (sin contar el aprisco).
    - puntos_corregidos: son los datos de los GPS con los puntos replicados cuando falta información.
  - csv: tiene la información de las tablas de atributos de las capas de parcelas, zonas, resto y todo unido anteriores desagregadas por GPS y agregadas por tipo.
